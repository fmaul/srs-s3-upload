import { DeleteObjectCommand, PutObjectCommand, S3Client } from '@aws-sdk/client-s3';
import fs from 'fs';
import { S3_ACCESS_KEY_ID, S3_ACCESS_KEY_SECRET, S3_ENDPOINT } from './env';

export const createS3Client = () => {
  if (!S3_ACCESS_KEY_ID) {
    throw new Error('S3_ACCESS_KEY_ID is not set.');
  }
  if (!S3_ACCESS_KEY_SECRET) {
    throw new Error('S3_ACCESS_KEY_SECRET is not set.');
  }

  return new S3Client({
    region: 'auto',
    endpoint: S3_ENDPOINT,
    credentials: {
      accessKeyId: S3_ACCESS_KEY_ID,
      secretAccessKey: S3_ACCESS_KEY_SECRET,
    },
  });
};

export const uploadFile = async (
  s3Client: S3Client,
  sourceFilePath: string,
  bucket: string,
  targetKey: string,
  contentType: string
) => {
  const fileStream = fs.createReadStream(sourceFilePath);
  console.log(`- Uploading File: ${sourceFilePath} to ${bucket}:${targetKey}`);

  return await s3Client.send(
    new PutObjectCommand({
      Bucket: bucket,
      Key: targetKey,
      Body: fileStream,
      ACL: 'public-read',
      ContentType: 'video/mp2t',
    })
  );
};

export const deleteFile = async (s3Client: S3Client, bucket: string, targetKey: string) => {
  console.log(`- Deleting File in S3: ${bucket}:${targetKey}`);

  await s3Client.send(
    new DeleteObjectCommand({
      Bucket: bucket,
      Key: targetKey,
    })
  );
};
