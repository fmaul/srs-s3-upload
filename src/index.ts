import express from 'express';
import { HLSUpdateEvent } from './types';
import { createS3Client, deleteFile, uploadFile } from './s3';
import { S3_BUCKET_NAME,  PORT, MAX_TS_FILES_TO_KEEP } from './env';

const app = express();
app.use(express.json()); // for parsing application/json

const s3Client = createS3Client();

const processOnHlsEvent = async (hlsEvent: HLSUpdateEvent) => {
  console.log(
    `Server ID: ${hlsEvent.server_id}, App: ${hlsEvent.app}, Stream: ${hlsEvent.stream}, SEQ: ${hlsEvent.seq_no}`
  );

  // Upload ts file
  await uploadFile(
    s3Client,
    `${hlsEvent.cwd}/${hlsEvent.file}`,
    S3_BUCKET_NAME,
    `${hlsEvent.url}`,
    'video/mp2t'
  );

  // Upload m3u8 file
  const m3u8S3Name = `${hlsEvent.m3u8_url}`;
  await uploadFile(s3Client, `${hlsEvent.cwd}/${hlsEvent.m3u8}`, S3_BUCKET_NAME, m3u8S3Name, 'application/x-mpegURL');

  // Delete OLD ts files
  const current = hlsEvent.seq_no;
  if (current >= MAX_TS_FILES_TO_KEEP) { // >= because we need to delete file 0.ts
    const fileToDelete = hlsEvent.url.replace(/[0-9]+\.ts$/, `${current - MAX_TS_FILES_TO_KEEP}.ts`);
    await deleteFile(s3Client, S3_BUCKET_NAME, `${fileToDelete}`);
  }
};

app.post('/api/v1/hls', async (req, res, next) => {
  // console.debug('POST /api/v1/hls called: Received HLS update event.');
  const hlsEvent = req.body as HLSUpdateEvent;
  await processOnHlsEvent(hlsEvent);
  res.send('0'); // srs needs 0 == OK
});

app.listen(PORT, async () => {
  console.log(`SRS S3 Uploader started on port ${PORT}. Waiting for events...`);
  console.log(`Uploading to S3 Bucket: ${S3_BUCKET_NAME}`);
});

// Global error handler - route handlers/middlewares which throw end up here
app.use((err) => {
  console.error(err);
});
