import 'dotenv/config';

export const S3_ENDPOINT = process.env.S3_ENDPOINT;
export const S3_ACCESS_KEY_ID = process.env.S3_ACCESS_KEY_ID;
export const S3_ACCESS_KEY_SECRET = process.env.S3_ACCESS_KEY_SECRET;
export const S3_BUCKET_NAME = process.env.S3_BUCKET_NAME || 'bucket';
export const MAX_TS_FILES_TO_KEEP = parseInt(process.env.MAX_TS_FILES_TO_KEEP || '20', 10);
export const PORT = process.env.PORT || 3000;
