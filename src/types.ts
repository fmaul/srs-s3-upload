/*
{"server_id":"vid-34g09wb","service_id":"4l00gw4n","action":"on_hls",
"client_id":"7lgl6020","ip":"127.0.0.1","vhost":"__defaultVhost__","app":
"live","tcUrl":"rtmp://localhost/live","stream":"livestream","param":"",
"duration":12.01,"cwd":"/Users/t3adminfmau/dev/srs/trunk",
"file":"./objs/nginx/html/live/livestream-80.ts","url":"live/livestream-80.ts",
"m3u8":"./objs/nginx/html/live/livestream.m3u8","m3u8_url":"live/livestream.m3u8",
"seq_no":80,"stream_url":"/live/livestream","stream_id":"vid-q015v6y"}
*/
export type HLSUpdateEvent = {
  server_id: string;
  service_id: string;
  action: string;
  client_id: string;
  ip: string;
  vhost: string;
  app: string;
  tcUrl: string;
  stream: string;
  param: string;
  duration: number;
  cwd: string;
  file: string;
  url: string;
  m3u8: string;
  m3u8_url: string;
  seq_no: number;
  stream_url: string;
  stream_id: string;
};
