FROM srs:latest

# Install Node.js
RUN apt-get -y update && apt-get install -y curl
RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash - && apt-get install -y nodejs

# Install the S3 upload tool
RUN mkdir /usr/local/srs/upload
RUN chmod 755 /usr/local/srs/upload
COPY . /usr/local/srs/upload
WORKDIR /usr/local/srs/upload
RUN npm install 

# Setup the startup script
WORKDIR /usr/local/srs
COPY entrypoint.sh /usr/local/srs/entrypoint.sh
RUN chmod 755 entrypoint.sh 

# Copy SRS config
COPY conf conf

CMD ["bash", "-c", "./entrypoint.sh"]
