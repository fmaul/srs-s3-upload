# Start S3 upload tool
cd /usr/local/srs/upload
npm start &

# Start SRS
cd /usr/local/srs
./objs/srs -c conf/mysrs.conf
